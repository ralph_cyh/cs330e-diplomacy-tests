
# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_eval, diplomacy_print

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    def test_read1(self):
        s = "A Madrid Hold\n"
        l = diplomacy_read(s)
        self.assertEqual(l[0], "A")
        self.assertEqual(l[1], "Madrid")
        self.assertEqual(l[2], "Hold")

    def test_read2(self):
        s = "A Madrid Move Mexico\n"
        l = diplomacy_read(s)
        self.assertEqual(l[0], "A")
        self.assertEqual(l[1], "Madrid")
        self.assertEqual(l[2], "Move")
        self.assertEqual(l[3], "Mexico")

    def test_read3(self):
        s = "D Paris Support B\n"
        l = diplomacy_read(s)
        self.assertEqual(l[0], "D")
        self.assertEqual(l[1], "Paris")
        self.assertEqual(l[2], "Support")
        self.assertEqual(l[3], "B")


    # ----
    # eval
    # ----

    def test_eval1(self):
        v = diplomacy_eval([["A", "Madrid","Hold"],["B", "Barcelona", "Hold"]])
        self.assertEqual(v, v)

    def test_eval2(self):
        v = diplomacy_eval([["A", "Madrid","Support", "C"],["B", "Barcelona", "Hold"], ["C", "Chicago", "Move", "Barcelona"]])
        self.assertEqual(v, v)

    def test_eval3(self):
        v = diplomacy_eval([["A", "London","Move", "Barcelona"],["B", "Barcelona", "Hold"]])
        self.assertEqual(v, v)

    # -----
    # print
    # -----


    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]\nB Madrid\nC London\n")
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, "A Madrid\nB Mexico\nC Hawaii\n")
        self.assertEqual(w.getvalue(), "A Madrid\nB Mexico\nC Hawaii\n")

    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, "A Germany\nB London\n")
        self.assertEqual(w.getvalue(), "A Germany\nB London\n")

    # -----
    # solve
    # -----
    """
    r a reader
    w a writer
    """

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Scottland\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Scottland\n")

    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC Mexico Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC Mexico\n")

    def test_solve6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC Mexico Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve7(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")

# ----
# main
# ----

if __name__ == "__main__":#pragma: no cover
    main()
